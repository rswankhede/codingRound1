<?php

/**
 Write a function that takes an array of integers and returns that array rotated by N positions
 using php
 */

/**
 * This function use for roated array by given 
 * position
 * @param array $inputArr
 * @param int $position
 * @return array $result
 */

function roatedArrayByPosition($inputArr, $position)
{
    $result = array();
    $temp1 = array();
    $temp2 = array();
    $inputArrLength = count($inputArr);
    $splitIndex  = $inputArrLength - $position;

    for ($i = 0; $i < $inputArrLength; $i++) {
        if ($splitIndex > $i) {
            array_push($temp1, $inputArr[$i]);
        } else {
            array_push($temp2, $inputArr[$i]);
        }
    }
    $result = array_merge($temp2, $temp1);
    return $result;
}

$inputArr = array(1, 2, 3, 4, 5, 6);
$getResultArr = roatedArrayByPosition($inputArr, 2);

echo '<pre>';
print_r($getResultArr);

// Output:
// Array
// (
//     [0] => 5
//     [1] => 6
//     [2] => 1
//     [3] => 2
//     [4] => 3
//     [5] => 4
// )

?>