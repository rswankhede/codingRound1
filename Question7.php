/**
 * 
 *  Write a simple program that reads a line from the keyboard and outputs the same line where
 *  every word is reversed. A word is defined as a continuous sequence of alphanumeric characters
 *  or hyphen (`-`). For instance, if the input is
 */

 function reverseString($input)
    {
        $chars = [];

        for ($i = 0; $i < strlen($input); $i++) {
            if (ctype_alpha($input[$i]) || $input[$i] == '-') {
                $chars[] = $input[$i];
            } else {
                $chars[] = '%s';
                $specialCharcter[] =  $input[$i];
            }
        }

        $implodInputString = implode('', $chars);
        $explodeFormattedString = explode('%s', $implodInputString);

        foreach ($explodeFormattedString as $key => $value) {
            $explodeFormattedString[$key] = strrev($value);
        }

        $implodInputString = implode("%s", $explodeFormattedString);
        return  vsprintf($implodInputString, $specialCharcter);
    }

    $input = 'We are at Ignite Solutions! Their email-id is careers@ignitesol.com';
    echo reverseString($input);
	//Output:- eW era ta etingI snoituloS! riehT di-liame si sreerac@losetingi.moc