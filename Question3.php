<?php

/**
Most languages have a built in sort method that will sort an array of strings alphabetically.
Demonstrate how to sort an array of strings by the length of each string, shortest strings first.
 */


/**
 * This function for sort the string by
 * word length
 * @param array $inputArr
 * @return array $inputArr
 */
function sortByLength($inputArr)
{
    array_multisort(array_map('strlen', $inputArr), $inputArr);
    return $inputArr;
}


$inputArr = "Most languages have a built in sort method";
print_r(sortByLength(explode(' ', $inputArr)));

// output :-> Array ( [0] => a [1] => in [2] => Most [3] => have [4] => sort [5] => built [6] => method [7] => languages )
